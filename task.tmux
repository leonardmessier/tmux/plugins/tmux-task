#!/usr/bin/env bash

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${CURRENT_DIR}/config/vars.sh"

set_normal_bindings() {
    tmux bind-key "${TMUX_TASK_DEFAULT_KEY}" \
      run "tmux new-window -a -c '#{pane_current_path}' -S ${CURRENT_DIR}/scripts/tmux-task.sh"
}

main() {
    set_normal_bindings
}

main
