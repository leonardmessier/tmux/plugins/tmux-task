#!/usr/bin/env bash

set -e

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$CURRENT_DIR/../config/vars.sh"

main() {
    local CMD="$TMUX_TASK_COMMAND"
    if [ -n "$TASK_FILTER" ]; then
      CMD+=" $TASK_FILTER"
    fi

    tmux rename-window "${TMUX_TASK_TITLE_ICON} ${TMUX_TASK_TITLE}"
    ${CMD}
}

main
